﻿using System;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;

namespace Otus.Teaching.Linq.ATM.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Старт приложения-банкомата...");

            var atmManager = CreateATMManager();

            System.Console.WriteLine();
            System.Console.WriteLine("1. Вывод информации о заданном аккаунте по логину и паролю;");
            atmManager.ShowUserInfo("snow", "111");

            System.Console.WriteLine();
            System.Console.WriteLine("2. Вывод данных о всех счетах заданного пользователя #1");
            atmManager.ShowAccountsInfo(1);

            System.Console.WriteLine();
            System.Console.WriteLine("3. Вывод данных о всех счетах пользователя #2, включая историю по каждому счёту");
            atmManager.ShowAccountsAndHistoryInfo(2);//или atmManager.ShowAccountsAndHistoryInfo2(2);

            System.Console.WriteLine();
            System.Console.WriteLine("4. Вывод данных о всех операциях пополенения счёта с указанием владельца каждого счёта");
            atmManager.ShowOperationInfo();

            System.Console.WriteLine();
            System.Console.WriteLine("5. Вывод данных о всех пользователях у которых на счёте сумма больше 1000");
            atmManager.ShowUsersWhichHaveCashMoreThen(1000);
            

            System.Console.WriteLine("Завершение работы приложения-банкомата...");
        }

        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();
                
            return new ATMManager(accounts, users, history);
        }
    }
}