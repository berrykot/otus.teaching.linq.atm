﻿using System.Collections.Generic;
using System;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Entities;
using System.Text;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }

        public IEnumerable<User> Users { get; private set; }

        public IEnumerable<OperationsHistory> History { get; private set; }

        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }

        //Вывод информации о заданном аккаунте по логину и паролю
        public void ShowUserInfo(string login, string pass)
        {
            var user = Users
                .FirstOrDefault(u => u.Login == login && u.Password == pass)
                ?? throw new Exception("Неверный логин ило пароль");

            Console.WriteLine($"Информация о пользователе {user.SurName} {user.FirstName} {user.MiddleName}");
            Console.WriteLine($"Телефон {user.Phone}, паспорт {user.PassportSeriesAndNumber} выдан {user.RegistrationDate:dd.MM.yyy}");
        }

        //Вывод данных о всех счетах заданного пользователя
        public void ShowAccountsInfo(int userId)
        {
            var accounts = Accounts
                .Where(a => a.UserId == userId);

            if (accounts.Count() == 0) Console.WriteLine("У клиента нет ни одного счёта");
            else Console.WriteLine("Информация о счетах клиента:");

            foreach (var acc in accounts)
            {
                Console.WriteLine($"Счет {acc.Id} открыт {acc.OpeningDate}. Текущий баланс {acc.CashAll}");
            }
        }

        //Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту
        public void ShowAccountsAndHistoryInfo2(int userId)
        {
            var accountQuery =
                from a in Accounts
                join h in History
                    on a.Id equals h.AccountId
                where a.UserId == userId
                orderby a.Id ascending
                select new
                {
                    Account = a,
                    History = h,
                };
            foreach (var info in accountQuery)
            {
                var oper = info.History.OperationType == OperationType.InputCash ? "снятие" : "пополнение";
                Console.WriteLine($"Счет {info.Account.Id} {info.History.OperationDate} {oper} {info.History.CashSum}");
            }
        }
        //Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту
        public void ShowAccountsAndHistoryInfo(int userId)
        {
            var accounts = Accounts
                .Where(a => a.UserId == userId);

            if (accounts.Count() == 0) Console.WriteLine("У клиента нет ни одного счёта");
            else Console.WriteLine("Информация о счетах клиента:");

            foreach (var acc in accounts)
            {
                Console.WriteLine($"Счет {acc.Id} открыт {acc.OpeningDate}. Текущий баланс {acc.CashAll}");
                var histories = History.Where(h => h.AccountId == acc.Id)
                    .OrderBy(h => h.AccountId)
                    .ThenByDescending(h => h.OperationDate);

                if (histories.Count() == 0) Console.WriteLine($"Клиент не совершал ни одной операции по счету {acc.Id}");
                else Console.WriteLine($"Клиент совершал следующие операции по счету {acc.Id}");

                foreach (var history in histories)
                {
                    var oper = history.OperationType == OperationType.InputCash ? "снятие" : "пополнение";
                    Console.WriteLine($"{history.OperationDate} {oper} {history.CashSum}");
                }
            }
        }

        //Вывод данных о всех операциях пополенения счёта с указанием владельца каждого счёта
        public void ShowOperationInfo()
        {
            var info = History
                .Join(Accounts, h => h.AccountId, a => a.Id, (History, Account) => new { History, Account })
                .Join(Users, ha => ha.Account.UserId, u => u.Id, (HistoryAndAccount, User) => new { HistoryAndAccount.History, HistoryAndAccount.Account, User })
                .OrderBy(i => i.Account.Id)
                .Select(i => $"Операция {i.History.OperationDate} {i.History.OperationType} {i.History.CashSum} по счету {i.Account.Id} клиента {i.User.SurName} {i.User.FirstName} {i.User.MiddleName}");

            var infoString = new StringBuilder()
                .AppendJoin(Environment.NewLine, info);

            Console.WriteLine(infoString.ToString());
        }

        //Вывод данных о всех пользователях у которых на счёте сумма больше N (N задаётся из вне и может быть любой);
        public void ShowUsersWhichHaveCashMoreThen(decimal moreThenCash)
        {
            var info = Users
                .Join(Accounts, u => u.Id, a => a.UserId, (User, Account) => new { User, Account })
                .Where(i => i.Account.CashAll > moreThenCash)
                .Select(i => $"{i.User.SurName} {i.User.FirstName} {i.User.MiddleName}")
                .Distinct();

            var infoString = new StringBuilder()
                .Append($"Пользователи имеющие на счету более {moreThenCash}")
                .AppendLine()
                .AppendJoin(Environment.NewLine, info);

            Console.WriteLine(infoString.ToString());
        }
    }
}